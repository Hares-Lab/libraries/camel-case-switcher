from tests.test_snake_to_camel import *
from tests.test_camel_case_to_camel import *
from tests.test_dict_processor import *

__all__ = \
[
    'Camel2SnakeAcronymProcessingTestCase',
    'Camel2SnakeCamelCaseToUnderscoreTestCase',
    'Camel2SnakeDifferentStyleNamesTestCase',
    'DictProcessorTests',
    'Snake2CamelDifferentStyleNamesTestCase',
    'Snake2CamelUnderscoreToCamelCaseTestCase',
]


if (__name__ == '__main__'):
    from unittest import main as unittest_main
    unittest_main()
